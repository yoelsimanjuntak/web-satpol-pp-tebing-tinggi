# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_satpolpp
# Generation Time: 2023-09-16 10:11:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Sambutan & Foto','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(2,'TxtWelcome2','Struktur Organisasi','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','STRUKTURORGANISASI.jpeg',NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(45,'TxtPopup1','Tugas Pokok dan Fungsi','PDF',NULL,'rekomsaranakesehatan.pdf',NULL,NULL),
	(46,'TxtPopup1','Maklumat Pelayanan','IMG',NULL,'rekomsaranakesehatan1.pdf',NULL,NULL),
	(47,'TxtPopup1','Standar Pelayanan','PDF',NULL,'rekomsaranakesehatan2.pdf',NULL,NULL),
	(49,'TxtPopup2','Kesehatan Masyarakat','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','Pencegahan & Pengendalian Penyakit','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','Pelayanan & Sumber Daya Kesehatan','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup3','Instalasi Farmasi','DB','select * from munit where UnitTipe = \'FARMASI\' order by UnitNama','UnitNama','Uniq',NULL),
	(53,'TxtPopup3','Puskesmas','DB','select * from munit where UnitTipe = \'PUSKESMAS\' order by UnitNama','UnitNama','Uniq',NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2022-11-28 22:46:06','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(2,'2022-11-28 22:46:22','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(3,'2022-11-28 22:46:29','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(4,'2022-11-28 22:48:58','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(5,'2022-11-28 22:49:44','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(6,'2022-11-29 07:10:11','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(7,'2022-11-29 07:22:43','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(8,'2022-11-29 07:25:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(9,'2022-11-29 07:25:53','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(10,'2022-11-29 07:27:04','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(11,'2022-11-29 07:31:14','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(12,'2022-11-29 07:31:26','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(13,'2022-11-29 07:31:55','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(14,'2022-11-29 07:32:19','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(15,'2022-11-29 07:32:51','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(16,'2022-11-29 07:33:28','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(17,'2022-11-29 07:40:36','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(18,'2022-11-29 07:53:31','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(19,'2022-11-29 07:54:08','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(20,'2022-11-29 07:55:02','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(21,'2022-11-29 07:55:13','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(22,'2022-11-29 07:55:17','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(23,'2022-11-29 07:57:55','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(24,'2022-11-29 08:00:43','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(25,'2022-11-29 08:04:21','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(26,'2022-11-29 08:04:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(27,'2022-11-29 08:06:38','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(28,'2022-11-29 08:07:35','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(29,'2022-11-29 08:08:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(30,'2022-11-29 08:09:54','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(31,'2022-11-29 08:12:11','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(32,'2022-11-29 08:45:30','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(33,'2022-11-29 08:47:27','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(34,'2022-11-29 08:48:32','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(35,'2022-11-29 08:49:03','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(36,'2022-11-29 08:49:22','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(37,'2022-11-29 08:49:38','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(38,'2022-11-29 08:49:49','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(39,'2022-11-29 08:50:20','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(40,'2022-11-29 08:51:04','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(41,'2022-11-29 08:51:39','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(42,'2022-11-29 08:52:06','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(43,'2022-11-29 08:52:28','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(44,'2022-11-29 08:52:48','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(45,'2022-11-29 08:53:44','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(46,'2022-11-29 08:53:54','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(47,'2022-11-29 08:55:03','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(48,'2022-11-29 08:55:26','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(49,'2022-11-29 08:57:14','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(50,'2022-11-29 08:57:24','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(51,'2022-11-29 08:57:32','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(52,'2022-11-29 08:57:40','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(53,'2022-11-29 08:58:29','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(54,'2022-11-29 08:58:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(55,'2022-11-29 08:59:48','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(56,'2022-11-29 09:00:47','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(57,'2022-11-29 09:01:38','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(58,'2022-11-29 09:01:53','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(59,'2022-11-29 09:02:17','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(60,'2022-11-29 09:02:54','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(61,'2022-11-29 09:03:09','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(62,'2022-11-29 09:04:22','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(63,'2022-11-29 09:04:33','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(64,'2022-11-29 09:05:27','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(65,'2022-11-29 09:05:51','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(66,'2022-11-29 09:06:20','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(67,'2022-11-29 09:06:39','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(68,'2022-11-29 09:08:51','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(69,'2022-11-29 09:09:29','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(70,'2022-11-29 09:09:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(71,'2022-11-29 09:10:17','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(72,'2022-11-29 09:11:28','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(73,'2022-11-29 09:11:37','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(74,'2022-11-29 09:30:28','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(75,'2022-11-29 09:31:31','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(76,'2022-11-29 09:34:00','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(77,'2022-11-29 09:34:39','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(78,'2022-11-29 09:35:16','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(79,'2022-11-29 09:38:13','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(80,'2022-11-29 09:38:19','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(81,'2022-11-29 09:38:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(82,'2022-11-29 09:39:02','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(83,'2022-11-29 09:40:24','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(84,'2022-11-29 09:40:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(85,'2022-11-29 09:40:54','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(86,'2022-11-29 09:40:59','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(87,'2022-11-29 09:41:23','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(88,'2022-11-29 09:42:38','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(89,'2022-11-29 09:43:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(90,'2022-11-29 09:44:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(91,'2022-11-29 09:45:20','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(92,'2022-11-29 09:46:34','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(93,'2022-11-29 09:47:02','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(94,'2022-11-29 09:47:09','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(95,'2022-11-29 09:47:19','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(96,'2022-11-29 09:47:26','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(97,'2022-11-29 09:47:46','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(98,'2022-11-29 09:48:00','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(99,'2022-11-29 09:48:34','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(100,'2022-11-29 09:48:58','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(101,'2022-11-29 09:49:04','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(102,'2022-11-29 09:49:11','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(103,'2022-11-29 09:49:56','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(104,'2022-11-29 09:50:38','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(105,'2022-11-29 09:50:49','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(106,'2022-11-29 09:51:35','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(107,'2022-11-29 09:52:56','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(108,'2022-11-29 09:53:24','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(109,'2022-11-29 09:53:30','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(110,'2022-11-29 09:53:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(111,'2022-11-29 09:53:48','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(112,'2022-11-29 09:53:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(113,'2022-11-29 09:54:07','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(114,'2022-11-29 09:54:12','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(115,'2022-11-29 09:54:51','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(116,'2022-11-29 09:54:59','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(117,'2022-11-29 09:55:12','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(118,'2022-11-29 09:55:36','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(119,'2022-11-29 09:55:50','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(120,'2022-11-29 09:55:59','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(121,'2022-11-29 09:56:10','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(122,'2022-11-29 09:56:16','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(123,'2022-11-29 09:56:26','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(124,'2022-11-29 09:56:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(125,'2022-11-29 09:56:59','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(126,'2022-11-29 09:57:33','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(127,'2022-11-29 09:57:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(128,'2022-11-29 09:58:04','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(129,'2022-11-29 09:58:22','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(130,'2022-11-29 09:58:32','http://localhost/tt-satpolpp/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(131,'2022-11-29 09:58:41','http://localhost/tt-satpolpp/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(132,'2022-11-29 09:58:41','http://localhost/tt-satpolpp/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(133,'2022-11-29 09:58:47','http://localhost/tt-satpolpp/site/setting/unit.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(134,'2022-11-29 09:59:03','http://localhost/tt-satpolpp/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(135,'2022-11-29 09:59:08','http://localhost/tt-satpolpp/site/setting/homepage.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(136,'2022-11-29 09:59:25','http://localhost/tt-satpolpp/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(137,'2022-11-29 09:59:45','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(138,'2022-11-29 10:02:18','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(139,'2022-11-29 10:02:46','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(140,'2022-11-29 10:02:46','http://localhost/tt-satpolpp/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(141,'2022-11-29 10:02:48','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(142,'2022-11-29 10:04:20','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(143,'2022-11-29 10:04:20','http://localhost/tt-satpolpp/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(144,'2022-11-29 10:04:23','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(145,'2022-11-29 10:05:39','http://localhost/tt-satpolpp/site/post/add/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(146,'2022-11-29 10:05:39','http://localhost/tt-satpolpp/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(147,'2022-11-29 10:05:43','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(148,'2022-11-29 10:05:58','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(149,'2022-11-29 10:08:15','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(150,'2022-11-29 10:08:23','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(151,'2022-11-29 10:08:44','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(152,'2022-11-29 10:09:42','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(153,'2022-11-29 10:10:07','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(154,'2022-11-29 10:10:36','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(155,'2022-11-29 10:10:53','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(156,'2022-11-29 10:11:13','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(157,'2022-11-29 10:12:37','http://localhost/tt-satpolpp/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(158,'2022-11-29 10:12:42','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(159,'2022-11-29 10:12:44','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(160,'2022-11-29 10:12:56','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(161,'2022-11-29 10:12:56','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(162,'2022-11-29 10:12:57','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(163,'2022-11-29 10:13:08','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(164,'2022-11-29 10:13:08','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(165,'2022-11-29 10:13:09','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(166,'2022-11-29 10:13:21','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(167,'2022-11-29 10:13:21','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(168,'2022-11-29 10:13:29','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(169,'2022-11-29 10:13:40','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(170,'2022-11-29 10:13:40','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(171,'2022-11-29 10:13:42','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(172,'2022-11-29 10:13:51','http://localhost/tt-satpolpp/site/post/add/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(173,'2022-11-29 10:13:51','http://localhost/tt-satpolpp/site/post/index/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(174,'2022-11-29 10:15:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(175,'2022-11-29 10:16:12','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(176,'2022-11-29 10:16:17','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(177,'2022-11-29 10:16:43','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(178,'2022-11-29 10:17:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(179,'2022-11-29 10:17:44','http://localhost/tt-satpolpp/site/post/index/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(180,'2022-11-29 10:18:11','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(181,'2022-11-29 10:18:12','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(182,'2022-11-29 10:18:15','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(183,'2022-11-29 10:18:32','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(184,'2022-11-29 10:19:26','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(185,'2022-11-29 10:20:13','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(186,'2022-11-29 10:21:32','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(187,'2022-11-29 10:21:35','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(188,'2022-11-29 10:22:35','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(189,'2022-11-29 10:26:26','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(190,'2022-11-29 10:26:49','http://localhost/tt-satpolpp/site/home/post/index.html.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(191,'2022-11-29 10:27:04','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(192,'2022-11-29 10:27:05','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(193,'2022-11-29 10:28:51','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(194,'2022-11-29 10:28:51','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(195,'2022-11-29 10:29:23','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(196,'2022-11-29 10:29:24','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(197,'2022-11-29 10:30:30','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(198,'2022-11-29 10:30:30','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(199,'2022-11-29 10:31:17','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(200,'2022-11-29 10:31:17','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(201,'2022-11-29 10:31:58','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(202,'2022-11-29 10:31:58','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(203,'2022-11-29 10:31:58','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(204,'2022-11-29 10:31:58','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(205,'2022-11-29 10:31:58','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(206,'2022-11-29 10:32:53','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(207,'2022-11-29 10:32:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(208,'2022-11-29 10:32:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(209,'2022-11-29 10:32:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(210,'2022-11-29 10:32:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(211,'2022-11-29 10:33:09','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(212,'2022-11-29 10:33:09','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(213,'2022-11-29 10:33:09','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(214,'2022-11-29 10:33:09','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(215,'2022-11-29 10:33:09','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(216,'2022-11-29 10:33:22','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(217,'2022-11-29 10:33:22','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(218,'2022-11-29 10:33:22','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(219,'2022-11-29 10:33:22','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(220,'2022-11-29 10:33:22','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(221,'2022-11-29 10:33:27','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(222,'2022-11-29 10:33:27','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(223,'2022-11-29 10:33:27','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(224,'2022-11-29 10:33:27','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(225,'2022-11-29 10:33:27','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(226,'2022-11-29 10:33:35','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(227,'2022-11-29 10:33:35','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(228,'2022-11-29 10:33:35','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(229,'2022-11-29 10:33:35','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(230,'2022-11-29 10:33:35','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(231,'2022-11-29 10:33:47','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(232,'2022-11-29 10:33:47','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(233,'2022-11-29 10:33:47','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(234,'2022-11-29 10:33:47','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(235,'2022-11-29 10:33:47','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(236,'2022-11-29 10:33:53','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(237,'2022-11-29 10:33:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(238,'2022-11-29 10:33:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(239,'2022-11-29 10:33:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(240,'2022-11-29 10:33:53','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(241,'2022-11-29 10:34:06','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(242,'2022-11-29 10:34:07','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(243,'2022-11-29 10:34:13','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(244,'2022-11-29 10:34:13','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(245,'2022-11-29 10:34:15','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(246,'2022-11-29 10:34:15','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-02.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(247,'2022-11-29 10:34:15','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-03.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(248,'2022-11-29 10:34:15','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(249,'2022-11-29 10:34:15','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-04.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(250,'2022-11-29 10:34:27','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(251,'2022-11-29 10:34:27','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(252,'2022-11-29 10:34:34','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(253,'2022-11-29 10:34:34','http://localhost/tt-satpolpp/site/home/post/assets/images/meeting-01.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(254,'2022-11-29 10:36:35','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(255,'2022-11-29 10:36:49','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(256,'2022-11-29 10:37:36','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(257,'2022-11-29 10:38:08','http://localhost/tt-satpolpp/site/home/post/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(258,'2022-11-29 10:38:55','http://localhost/tt-satpolpp/site/home/post/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(259,'2022-11-29 10:39:09','http://localhost/tt-satpolpp/site/home/post/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(260,'2022-11-29 10:39:24','http://localhost/tt-satpolpp/site/home/post/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(261,'2022-11-29 10:39:27','http://localhost/tt-satpolpp/site/home/post/2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(262,'2022-11-29 10:39:36','http://localhost/tt-satpolpp/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(263,'2022-11-29 10:41:46','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(264,'2022-11-29 10:43:02','http://localhost/tt-satpolpp/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(265,'2022-11-29 10:43:04','http://localhost/tt-satpolpp/site/post/add/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(266,'2022-11-29 10:43:30','http://localhost/tt-satpolpp/site/post/add/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(267,'2022-11-29 10:43:31','http://localhost/tt-satpolpp/site/post/index/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(268,'2022-11-29 10:43:42','http://localhost/tt-satpolpp/site/home/post/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(269,'2022-11-29 10:43:45','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(270,'2022-11-29 10:43:54','http://localhost/tt-satpolpp/site/home/page/demo-2.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(271,'2022-11-29 10:43:56','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(272,'2022-11-29 10:45:06','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(273,'2022-11-29 10:45:11','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(274,'2022-11-29 10:45:43','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(275,'2022-11-29 10:46:56','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(276,'2022-11-29 10:46:56','http://localhost/tt-satpolpp/site/home/page/assets/images/single-meeting.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(277,'2022-11-29 10:47:59','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(278,'2022-11-29 10:48:00','http://localhost/tt-satpolpp/site/home/page/assets/images/single-meeting.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(279,'2022-11-29 10:48:18','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(280,'2022-11-29 10:48:18','http://localhost/tt-satpolpp/site/home/page/assets/images/single-meeting.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(281,'2022-11-29 10:49:41','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(282,'2022-11-29 10:49:41','http://localhost/tt-satpolpp/site/home/page/assets/images/single-meeting.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(283,'2022-11-29 10:50:04','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(284,'2022-11-29 10:50:04','http://localhost/tt-satpolpp/site/home/page/assets/images/single-meeting.jpg.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(285,'2022-11-29 10:52:59','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(286,'2022-11-29 10:53:14','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(287,'2022-11-29 11:51:19','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(288,'2022-11-29 11:52:10','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(289,'2022-11-29 11:52:36','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(290,'2022-11-29 11:52:57','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(291,'2022-11-29 11:53:20','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(292,'2022-11-29 11:53:29','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(293,'2022-11-29 11:53:37','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(294,'2022-11-29 11:54:20','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(295,'2022-11-29 11:54:48','http://localhost/tt-satpolpp/site/home/page/demo-3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(296,'2022-11-29 11:54:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(297,'2022-11-29 12:09:42','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'),
	(298,'2023-01-26 08:49:52','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(299,'2023-01-26 08:51:57','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(300,'2023-01-26 08:52:05','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(301,'2023-01-26 08:52:23','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(302,'2023-01-26 08:53:34','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(303,'2023-01-26 08:53:50','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(304,'2023-01-26 08:54:12','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(305,'2023-01-26 08:54:25','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(306,'2023-01-26 08:54:34','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(307,'2023-01-26 08:55:17','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(308,'2023-01-26 09:00:18','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(309,'2023-01-26 09:07:16','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(310,'2023-01-26 09:09:10','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(311,'2023-01-26 09:09:37','http://localhost/tt-satpolpp/site/user/dashboard.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(312,'2023-01-26 09:09:37','http://localhost/tt-satpolpp/site/user/login.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(313,'2023-01-26 09:09:41','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(314,'2023-01-26 09:17:00','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(315,'2023-01-26 09:20:02','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(316,'2023-01-26 09:24:46','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(317,'2023-01-26 09:24:55','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(318,'2023-01-26 09:25:03','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(319,'2023-01-26 09:29:58','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'),
	(320,'2023-05-01 09:42:03','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(321,'2023-05-01 09:42:24','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(322,'2023-05-01 09:42:27','http://localhost/tt-satpolpp/site/home/post/3.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(323,'2023-05-01 09:46:05','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(324,'2023-05-01 12:01:51','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(325,'2023-05-01 12:19:29','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(326,'2023-05-01 19:16:53','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(327,'2023-05-01 22:03:37','http://localhost/tt-satpolpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36'),
	(328,'2023-05-01 22:03:41','http://localhost/tt-satpolpp/site/home/post/1.jsp','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `ImgPath`, `ImgDesc`, `ImgShortcode`, `IsHeader`, `IsThumbnail`, `Description`)
VALUES
	(1,1,'barisan.jpeg','','',1,1,NULL),
	(2,2,'IMG-20210303-WA0020.jpeg','','',1,1,NULL),
	(3,3,'20160217080953-personil-satpol-pp-pemko-tebingtinggi-melakukan-aksi-bersih-bersih.jpeg','','',1,1,NULL),
	(4,4,'IMG-20210303-WA00201.jpeg','','',1,1,NULL),
	(5,5,'IMG-20210303-WA00202.jpeg','','',1,1,NULL),
	(6,6,'IMG-20210303-WA00203.jpeg','','',1,1,NULL),
	(7,7,'IMG-20210303-WA00204.jpeg','','',1,1,NULL),
	(8,8,'IMG-20210303-WA00205.jpeg','','',1,1,NULL),
	(9,9,'UPACARA_HARI_KORPRI_29_NOVEMBER_2022.pdf','','',1,1,NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostUnitID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `PostMetaTags`, `IsRunningText`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,NULL,'2022-11-29','Demo 1','demo-1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:02:46','admin','2022-11-29 10:02:46'),
	(2,1,NULL,'2022-11-29','Demo 2','demo-2','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,0,1,NULL,0,NULL,'admin','2022-11-29 10:04:20','admin','2022-11-29 10:04:20'),
	(3,1,NULL,'2022-11-29','Demo 3','demo-3','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,0,18,NULL,0,NULL,'admin','2022-11-29 10:05:39','admin','2022-11-29 10:05:39'),
	(4,2,NULL,'2022-11-29','Lorem Ipsum','lorem-ipsum',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:12:56','admin','2022-11-29 10:12:56'),
	(5,2,NULL,'2022-11-29','Lorem Ipsum','lorem-ipsum',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:13:08','admin','2022-11-29 10:13:08'),
	(6,2,NULL,'2022-11-29','Lorem Ipsum','lorem-ipsum',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:13:21','admin','2022-11-29 10:13:21'),
	(7,2,NULL,'2022-11-29','Lorem Ipsum','lorem-ipsum',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:13:40','admin','2022-11-29 10:13:40'),
	(8,2,NULL,'2022-11-29','Lorem Ipsum','lorem-ipsum',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:13:51','admin','2022-11-29 10:13:51'),
	(9,3,NULL,'2022-11-29','Testing','testing',NULL,NULL,NULL,0,0,NULL,0,NULL,'admin','2022-11-29 10:43:30','admin','2022-11-29 10:43:30');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SATPOL PP'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Website Resmi\r\nSatuan Polisi Pamong Praja\nKota Tebing Tinggi'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Satuan Polisi Pamong Praja'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Tanjung Marulak, Kec. Rambutan, Kota Tebing Tinggi, Sumatera Utara 20988'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','--'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','---'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','--'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(11) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(11) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(11) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `CompanyID`, `Name`, `IdentityNo`, `BirthDate`, `ReligionID`, `Gender`, `Address`, `PhoneNumber`, `EducationID`, `UniversityName`, `FacultyName`, `MajorName`, `IsGraduated`, `GraduatedDate`, `YearOfExperience`, `RecentPosition`, `RecentSalary`, `ExpectedSalary`, `CVFilename`, `ImageFilename`, `RegisteredDate`)
VALUES
	(1,'admin','administrator@dinkes.tebingtinggikota.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2022-11-29 09:58:41','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
