<?php
class Home extends MY_Controller {

  public function index() {
    $data['title'] = 'Beranda';

    $this->load->model('mpost');

    $data['berita'] = $this->mpost->search(9,"",1);
    $data['galeri'] = $this->mpost->search(10,"",2);
    $data['dokumen'] = $this->mpost->search(5,"",3);
		//$this->template->set('title', 'Home');
		//$this->template->load('frontend-mediplus' , 'home/index', $data);
    $this->template->load('edumeet' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  public function index_temp() {
    $data['title'] = 'Beranda';
		$this->template->load('frontend-mediplus' , 'home/index_temp', $data);
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $data['berita'] = $this->mpost->search(5,"",1);
    $this->template->load('edumeet' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function post_old($cat) {
    $data['title'] = 'Tautan';
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q." LIMIT 10 OFFSET $start")->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }

  public function post($cat) {
    $data['title'] = 'Tautan';
    $start = !empty($_GET['start'])&&$_GET['start']>0?$_GET['start']:0;
    $rcat = $this->db
    ->where(COL_POSTCATEGORYID, $cat)
    ->get(TBL__POSTCATEGORIES)
    ->row_array();
    if(empty($rcat)) {
      show_404();
      return false;
    }

    if(!empty($_GET['dateFrom'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." >= ", $_GET['dateFrom']);
    if(!empty($_GET['dateTo'])) $this->db->where(TBL__POSTS.".".COL_CREATEDON." <= ", $_GET['dateTo']);
    if(!empty($_GET['keyword'])) {
      $this->db->group_start();
      $this->db->like(TBL__POSTS.".".COL_POSTCONTENT, $_GET['keyword']);
      $this->db->or_like(TBL__POSTS.".".COL_POSTTITLE, $_GET['keyword']);
      $this->db->group_end();
    }

    $q = $this->db
    ->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner")
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner")
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat)
    ->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL__POSTS, TRUE);

    $data['res'] = $rpost = $this->db->query($q.(!empty($start)?" LIMIT 9 OFFSET $start":""))->result_array();
    $data['count'] = $this->db->query($q)->num_rows();
    $data['start'] = $start;

    if(!empty($rcat)) {
      $data['title'] = $rcat[COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $data['rcat'] = $rcat;
    $this->template->load('edumeet' , 'home/post', $data);
  }

  public function unit($id=null) {
    $this->load->model('mpost');
    $data['title'] = 'Unit Kerja';

    if(!empty($id)) {
      $runit = $this->db->where(COL_UNIQ, $id)->get(TBL_MUNIT)->row_array();
      if(!$runit) {
        show_error('Parameter tidak valid!');
        exit();
      }
      $data['data'] = $runit;
      $data['title'] = $runit[COL_UNITNAMA];
      $this->template->load('frontend-mediplus' , 'home/unit-detail', $data);
    } else {
      $runit = $this->db->order_by(COL_UNITNAMA, 'asc')->get(TBL_MUNIT)->result_array();
      $data['data'] = $runit;
      $this->template->load('frontend-mediplus' , 'home/unit', $data);
    }
  }
}
 ?>
