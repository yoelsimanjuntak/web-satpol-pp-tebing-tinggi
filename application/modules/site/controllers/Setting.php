<?php
class Setting extends MY_Controller
{
  public function __construct()
  {
      parent::__construct();
      if (!IsLogin()) {
          redirect('site/user/login');
      }
      if (GetLoggedUser()[COL_ROLEID]!=ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }
  }

  public function change() {
    $rsetting = $this->db->get(TBL__SETTINGS)->result_array();
    $this->db->trans_begin();
    try {
      foreach($rsetting as $r) {
        if(isset($_POST[$r[COL_SETTINGLABEL]])) {
          $res = $this->db->where(COL_SETTINGLABEL, $r[COL_SETTINGLABEL])->update(TBL__SETTINGS, array(COL_SETTINGVALUE=>$_POST[$r[COL_SETTINGLABEL]]));
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('Perubahan pengaturan berhasil.');
      exit();
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
  }

  public function homepage($uniq='') {
    $data['title'] = 'Homepage';
    if(!empty($uniq)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|pdf";
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $filename = '';
      if(!empty($_FILES) && !empty($_FILES['ContentDesc2'])){
        if(!$this->upload->do_upload('ContentDesc2')) {
          show_error($this->upload->display_errors());
          exit();
        }
        $file = $this->upload->data();
        $filename = $file['file_name'];
      }
      $rec = array(
        COL_CONTENTDESC1=>isset($_POST[COL_CONTENTDESC1])?$_POST[COL_CONTENTDESC1]:null,
        COL_CONTENTDESC3=>isset($_POST[COL_CONTENTDESC3])?$_POST[COL_CONTENTDESC3]:null,
        COL_CONTENTDESC4=>isset($_POST[COL_CONTENTDESC4])?$_POST[COL_CONTENTDESC4]:null
      );
      if(!empty($filename)) {
        $rec[COL_CONTENTDESC2] = $filename;
      }
      $res = $this->db->where(COL_UNIQ, $uniq)->update(TBL__HOMEPAGE, $rec);
      redirect('site/setting/homepage');
    } else {
      $this->template->load('backend' , 'setting/homepage', $data);
    }
  }

  public function unit() {
    $data['title'] = 'Unit Kerja';
    $data['res'] = $rdata = $this->db->order_by(COL_UNITNAMA)->get(TBL_MUNIT)->result_array();
    $this->template->load('backend' , 'setting/unit', $data);
  }

  public function unit_add() {
      if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/user/dashboard');
      }
      $user = GetLoggedUser();
      $data['title'] = "Unit Kerja";
      $data['edit'] = FALSE;

      if(!empty($_POST)) {
        $data = array(
          COL_UNITTIPE => $this->input->post(COL_UNITTIPE),
          COL_UNITNAMA => $this->input->post(COL_UNITNAMA),
          COL_UNITPIMPINAN => $this->input->post(COL_UNITPIMPINAN),
          COL_UNITPIMPINANNIP => $this->input->post(COL_UNITPIMPINANNIP),
          COL_UNITPIMPINANPANGKAT => $this->input->post(COL_UNITPIMPINANPANGKAT),
          COL_UNITALAMAT => $this->input->post(COL_UNITALAMAT),
          COL_UNITGOOGLELINK => $this->input->post(COL_UNITGOOGLELINK),
          COL_UNITTELP => $this->input->post(COL_UNITTELP),
          COL_UNITEMAIL => $this->input->post(COL_UNITEMAIL),
        );

        $this->db->trans_begin();
        try {
          $res = $this->db->insert(TBL_MUNIT, $data);
          if(!$res) {
            throw new Exception('Gagal menginput unit kerja.');
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Unit Kerja berhasil diinput.', array('redirect'=>site_url('site/setting/unit')));
        exit();
      }
      else {
        $this->template->load('backend' , 'setting/unit-form', $data);
      }
  }

  public function unit_edit($id) {
      if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/user/dashboard');
      }
      $user = GetLoggedUser();
      $data['title'] = "Unit Kerja";
      $data['edit'] = TRUE;
      $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MUNIT)->row_array();
      if(empty($rdata)) {
        show_error('Parameter tidak valid!');
        exit();
      }

      if(!empty($_POST)) {
        $data = array(
          COL_UNITTIPE => $this->input->post(COL_UNITTIPE),
          COL_UNITNAMA => $this->input->post(COL_UNITNAMA),
          COL_UNITPIMPINAN => $this->input->post(COL_UNITPIMPINAN),
          COL_UNITPIMPINANNIP => $this->input->post(COL_UNITPIMPINANNIP),
          COL_UNITPIMPINANPANGKAT => $this->input->post(COL_UNITPIMPINANPANGKAT),
          COL_UNITALAMAT => $this->input->post(COL_UNITALAMAT),
          COL_UNITGOOGLELINK => $this->input->post(COL_UNITGOOGLELINK),
          COL_UNITTELP => $this->input->post(COL_UNITTELP),
          COL_UNITEMAIL => $this->input->post(COL_UNITEMAIL),
        );

        $this->db->trans_begin();
        try {
          $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MUNIT, $data);
          if(!$res) {
            throw new Exception('Gagal menginput unit kerja.');
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Unit Kerja berhasil diperbarui.', array('redirect'=>site_url('site/setting/unit')));
        exit();
      }
      else {
        $this->template->load('backend' , 'setting/unit-form', $data);
      }
  }

  function unit_delete(){
      if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        redirect('site/user/dashboard');
      }
      $data = $this->input->post('cekbox');
      $deleted = 0;
      foreach ($data as $datum) {
        $this->db->delete(TBL_MUNIT, array(COL_UNIQ => $datum));
        $deleted++;
      }
      if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
      }else{
        ShowJsonError("Tidak ada dihapus");
      }
  }
}
