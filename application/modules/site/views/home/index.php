<section class="section main-banner" id="top" data-section="section1">
    <video autoplay muted loop id="bg-video">
        <source src="<?=MY_IMAGEURL?>video-bg-tebingtinggi.mp4" type="video/mp4" />
    </video>

    <div class="video-overlay header-text">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="caption">
            <h6>SELAMAT DATANG DI</h6>
            <h2 style="font-size: 24px !important"><?=nl2br($this->setting_web_desc)?></h2>
            <!--<p>This is an edu meeting HTML CSS template provided by <a rel="nofollow" href="https://templatemo.com/page/1" target="_blank">TemplateMo website</a>. This is a Bootstrap v5.1.3 layout. The video background is taken from Pexels website, a group of young people by <a rel="nofollow" href="https://www.pexels.com/@pressmaster" target="_blank">Pressmaster</a>.</p>
            <div class="main-button-red">
              <div class="scroll-to-section"><a href="#contact">Join Us Now!</a></div>
            </div>-->
        </div>
            </div>
          </div>
        </div>
    </div>
</section>
<section class="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="owl-service-item owl-carousel">

          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-01.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Drs. Yustin Bernat Hutapea</h4>
              <p>Kepala Satuan Pamong Praja</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-02.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Ummy Syahti, SE, MM</h4>
              <p>Sekretaris</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-03.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Raja Amirudin Hasibuan , SH</h4>
              <p>Kabid Penegakan Perda dan Perwa</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-04.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Sariahman Damanik, S.HI,M.Si</h4>
              <p>Kabid Ketentraman dan Ketertiban Umum</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-05.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Talenta Siagian, S.Sos</h4>
              <p>Kasubbag Umum dan Kepegawaian</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-06.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Meriati Sipayung, SE, M.Si</h4>
              <p>Kasubbag Perencanaan dan Keuangan</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-07.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Mendar Haro Rajagukguk, SE</h4>
              <p>Kepala Seksi Kerja Sama</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-08.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Darman Huri</h4>
              <p>Kepala Seksi Ketertiban Umum</p>
            </div>
          </div>
          <div class="item">
            <div class="icon" style="max-width: 100px !important;">
              <img src="<?=MY_IMAGEURL.'img-profile-09.png'?>" style="height: 100px !important; width: auto; border-radius: 50%">
            </div>
            <div class="down-content">
              <h4 style="margin-bottom: 0 !important">Iswandi Saragih, SH</h4>
              <p>Kepala Seksi Operasional dan Pengendalian</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="upcoming-meetings" id="meetings">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-heading">
          <h2>BERITA TERKINI</h2>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <?php
          foreach($berita as $b) {
            $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
            $img = $this->db->where(COL_ISTHUMBNAIL,1)->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
            ?>
            <div class="col-lg-4">
              <div class="meeting-item">
                <div class="thumb">
                  <div class="price">
                    <span><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></span>
                  </div>
                  <div style="
                  height: 250px;
                  width: 100%;
                  background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                  background-size: cover;
                  background-repeat: no-repeat;
                  background-position: center;
                  ">
                </div>
                </div>
                <div class="down-content">
                  <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h4><?=$b[COL_POSTTITLE]?></h4></a>
                  <p style="margin-left: 0 !important"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                </div>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
if(!empty($galeri)) {
  ?>
  <section class="our-courses" id="courses">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="section-heading">
            <h2>GALERI</h2>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="owl-courses-item owl-carousel">
            <?php
            foreach($galeri as $r) {
              $rfiles = $this->db
              ->where(COL_POSTID, $r[COL_POSTID])
              ->get(TBL__POSTIMAGES)
              ->result_array();
              foreach($rfiles as $f) {
                ?>
                <div class="item">
                  <div class="single-pf" style="width: 300px; height: 200px; background-image: url('<?=MY_UPLOADURL.$f[COL_IMGPATH]?>'); background-size: cover">
                    <!--<a href="<?=site_url('site/ajax/popup-galeri/'.$f[COL_POSTIMAGEID])?>" class="btn btn-popup-galeri">LIHAT</a>-->
                  </div>
                  <div class="down-content">
                    <h4><?=$r[COL_POSTTITLE]?></h4>
                  </div>
                </div>

                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
}
?>
