<section class="heading-page header-text" id="top" style="background-image: url('<?=MY_IMAGEURL.'img-bg-home3.png'?>')">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h6>PUBLIKASI</h6>
        <h2><?=$rcat[COL_POSTCATEGORYNAME]?></h2>
      </div>
    </div>
  </div>
</section>
<section class="meetings-page" id="meetings" style="background: #f9f9f9 !important; padding-top: 60px !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="row grid">
              <?php
              foreach($res as $b) {
                $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                $img = $this->db->where(COL_POSTID, $b[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                ?>
                <div class="col-lg-4 templatemo-item-col all">
                  <div class="meeting-item">
                    <div class="thumb">
                      <div class="price">
                        <span><?=date('d-m-Y', strtotime($b[COL_CREATEDON]))?></span>
                      </div>
                      <div style="
                      height: 250px;
                      width: 100%;
                      background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_IMGPATH]:MY_IMAGEURL.'no-image.png'?>');
                      background-size: cover;
                      background-repeat: no-repeat;
                      background-position: center;
                      ">
                    </div>
                    </div>
                    <div class="down-content">
                      <a href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>"><h4><?=$b[COL_POSTTITLE]?></h4></a>
                      <?php
                      if(!empty($b[COL_POSTCONTENT])) {
                        ?>
                        <p style="margin-left: 0 !important"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                        <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
