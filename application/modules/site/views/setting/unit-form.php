
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="font-weight-light text-sm"> Form</small></h1>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-header">
            <a href="<?=site_url('site/setting/unit')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>NAMA</label>
                      <input type="text" class="form-control" name="<?=COL_UNITNAMA?>" value="<?=!empty($data[COL_UNITNAMA]) ? $data[COL_UNITNAMA] : ''?>" placeholder="Nama Unit" required />
                    </div>
                    <div class="col-sm-6">
                      <label>TIPE</label>
                      <select name="<?=COL_UNITTIPE?>" class="form-control" required>
                        <option value="FARMASI" <?=!empty($data[COL_UNITTIPE])&&$data[COL_UNITTIPE]=='FARMASI'?'selected':''?>>FARMASI</option>
                        <option value="PUSKESMAS" <?=!empty($data[COL_UNITTIPE])&&$data[COL_UNITTIPE]=='PUSKESMAS'?'selected':''?>>PUSKESMAS</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>TELP</label>
                      <input type="text" class="form-control" name="<?=COL_UNITTELP?>" value="<?=!empty($data[COL_UNITTELP]) ? $data[COL_UNITTELP] : ''?>" placeholder="Telepon / Kontak" required />
                    </div>
                    <div class="col-sm-6">
                      <label>EMAIL</label>
                      <input type="text" class="form-control" name="<?=COL_UNITEMAIL?>" value="<?=!empty($data[COL_UNITEMAIL]) ? $data[COL_UNITEMAIL] : ''?>" placeholder="Email" required />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>ALAMAT</label>
                  <textarea class="form-control" name="<?=COL_UNITALAMAT?>" placeholder="Alamat Unit Kerja"><?=!empty($data[COL_UNITALAMAT])?$data[COL_UNITALAMAT]:''?></textarea>
                </div>
                <div class="form-group">
                  <label>GOOGLE MAPS</label>
                  <textarea class="form-control" name="<?=COL_UNITGOOGLELINK?>" placeholder="Link Google Maps"><?=!empty($data[COL_UNITGOOGLELINK])?$data[COL_UNITGOOGLELINK]:''?></textarea>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label>PIMPINAN</label>
                  <input type="text" class="form-control" name="<?=COL_UNITPIMPINAN?>" value="<?=!empty($data[COL_UNITPIMPINAN]) ? $data[COL_UNITPIMPINAN] : ''?>" placeholder="Nama Pimpinan" required />
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>NIP PIMPINAN</label>
                      <input type="text" class="form-control" name="<?=COL_UNITPIMPINANNIP?>" value="<?=!empty($data[COL_UNITPIMPINANNIP]) ? $data[COL_UNITNAMA] : ''?>" placeholder="198502xx xxxxxx x xxx" required />
                    </div>
                    <div class="col-sm-6">
                      <label>PANGKAT PIMPINAN</label>
                      <input type="text" class="form-control" name="<?=COL_UNITPIMPINANPANGKAT?>" value="<?=!empty($data[COL_UNITPIMPINANPANGKAT]) ? $data[COL_UNITPIMPINANPANGKAT] : ''?>" placeholder="Pembina / IV.a" required />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
$(document).ready(function() {
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          console.log(res);
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
